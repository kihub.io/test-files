'use strict'

const path = require('path')
const fs = require('fs')

const dataDir = n => path.resolve(__dirname, 'data', n)

const filesByType = (dir, ext) => fs.readdirSync(dataDir(dir))
	.filter(fn => fn.endsWith(ext))
	.map(fn => path.resolve(dataDir(dir), fn));

const filesByTypeRecurseOne = (dir, ext) => fs
	.readdirSync(dataDir(dir))
	.map(fn => path.resolve(dataDir(dir), fn))
	.filter(dir2 => fs.statSync(dir2).isDirectory())
	.reduce((fileList, dir2) => 
		fileList.concat(
			fs.readdirSync(dir2)
				.filter(fn => fn.endsWith(ext))
				.map(fn => path.resolve(dir2, fn))
		),
	[]);

module.exports = {
	// Schematic libraries
	schemLibs: filesByType('schem-libs', 'lib').concat(filesByType('std-schem-libs', 'lib')),

	// Schematic files
	schematics: filesByTypeRecurseOne('schematic', 'sch'),

	// Project files
	projects: filesByTypeRecurseOne('schematic', 'pro'),

	// PCB
	pcbs: filesByType('pcb', 'kicad_pcb'),

	// Footprint libs
	fplibs: filesByType('footprint-libs', 'pretty')
}
