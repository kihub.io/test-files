This repository contains various KiCAD files taken from public sources, for the sole purpose of compatibility testing.

Sources:
https://github.com/OLIMEX/OLINUXINO.git
https://github.com/AnaviTechnology/anavi-thermometer.git
https://github.com/AnaviTechnology/anavi-gas-detector.git
https://github.com/AnaviTechnology/anavi-door-phat.git
https://github.com/AnaviTechnology/anavi-light-controller
https://github.com/AntonioMR/ATMEGA328-Motor-Board.git
https://github.com/jonpe960/blixten.git
https://github.com/ciaa/Hardware.git
https://kicad.github.io/symbols
